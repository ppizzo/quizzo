#!/usr/bin/python3
#
# Quiz taking software. Reads questions from YAML files.
#
# 2018, Pietro Pizzo <pietro.pizzo@gmail.com>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
########################################################################

VERSION="0.1.0"

import os, sys, random, readline
import yaml
from termcolor import colored
from argparse import ArgumentParser

# ANSI escape sequences
#REW="\033[1A\033[12C"
REW="\033[1A\033[1C"

random.seed()

# Read YAML file
def readfile(filename):
    try:
        finput = open(filename)
        return yaml.load(finput)
    except Exception as e:
        print(colored("Error: {0}".format(e), "red"), file=sys.stderr)
        return None

# Check if 'a' is the correct answer for 'q'.
# 'q' is a generic entry read from YAML file
def check(question, answer):
    # The right answer
    right = question["a"]
    # Answers in question can be strings or list of strings:
    # in this case either answer will work
    if type(right) == str:
        return right.lower() == answer.lower()
    elif type(right) == list:
        for a in right:
            if a.lower() == answer.lower():
                return True
        return False
    else:
        print(colored("Error: answer type '{}' not valid.".format(type(right)), "red"), file=sys.stderr)
        return False

# Main function
def run(filename, count):
    score = 0
    os.system("clear")

    # Read questions from YAML file
    qa = readfile(filename)
    if qa == None:
        return 1

    # Ask 'count' questions
    q, qold = None, None
    for i in range(count):

        # Make sure we do not ask the same question two consecutive times
        while q == qold:
            q = random.choice(qa)
        qold = q

        # Write question and read answer
        a = input("[  ] "+"{}/{}".format(i+1, count).ljust(5)+": {} ? ".format(q["q"]))

        # Check answer
        if check(q, a):
            print(REW+colored("OK", "green", attrs=["bold"]))
            score = score +1
        else:
            print(REW+colored("NO", "red", attrs=["bold"]))

    # Quiz finished: let's print the score
    mark = round(score*10/count, 2)
    print(colored("\nScore : {}/{}".format(score, count), attrs=["bold"]))
    print(colored("Mark  : {}".format(mark), attrs=["bold"]))
    if (score == count):
        print(colored("Congratulations! No errors!!!", attrs=["bold"]))

    return 0

# Main called from command line
if __name__ == "__main__":

    # Command line parsing
    parser = ArgumentParser(description="Quiz generator.")
    parser.add_argument("filename", help="YAML file containing questions and answers")
    parser.add_argument("-c", "--count", type=int, dest="count", default=10, help="number of questions to ask (default: %default)")
    parser.add_argument("-v", "--version", action="version", version="%(prog)s {0}".format(VERSION))
    args = parser.parse_args()

    # Invoke main function
    sys.exit(run(args.filename, args.count) or 0)
